package profile

import (
	"errors"
)

// ProfileRepository provides access to the profile store
type ProfileRepository interface {
	Index() int

	Store(profile *Profile) error

	Find(id int) (Profile, error)

	FindAll() []Profile

	Remove(id int) error
}

// Service is the interface that provides profile methods
type Service interface {
	CreateProfile(name string, userId int, path string) (int, error)

	LoadProfileById(id int) (Profile, error)

	LoadProfiles() []Profile

	RemoveProfile(id int) error
}

type profileService struct {
	profile ProfileRepository
}

func (p *profileService) CreateProfile(name string, userId int, path string) (int, error) {
	if name == "" || path == "" {
		return -1, ErrInvalidArgument
	}

	index := p.profile.Index()
	profile := &Profile{
		Id:     index,
		UserId: userId,
		Name:   name,
		Img:    path,
	}

	if err := p.profile.Store(profile); err != nil {
		return -1, err
	}

	return profile.Id, nil
}

func (p *profileService) LoadProfileById(id int) (Profile, error) {
	profile, err := p.profile.Find(id)
	if err != nil {
		return Profile{}, err
	}
	return profile, nil
}

func (p *profileService) LoadProfiles() []Profile {
	return p.profile.FindAll()
}

func (p *profileService) RemoveProfile(id int) error {
	if err := p.profile.Remove(id); err != nil {
		return err
	}
	return nil
}

func NewProfileService(profile ProfileRepository) *profileService {
	return &profileService{profile: profile}
}


// Profile is the entity and the dto
type Profile struct {
	Id     int    `json:"id"`
	UserId int    `json:"user_id"`
	Name   string `json:"name"`
	Img    string `json:"img"`
}

var ErrUnknownProfile = errors.New("could not find profile")
var ErrInvalidArgument = errors.New("invalid arguments")
