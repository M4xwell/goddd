package server

import (
	"../profile"
	"github.com/go-chi/chi"
	"log"
	"net/http"
)

type Server struct {
	Profile profile.Service

	router chi.Router
}

func NewServer(profile profile.Service) *Server {
	server := &Server{
		Profile: profile,
	}

	r := chi.NewRouter()
	r.User(accessContro)

	server.router = r

	return server
}

func ListenAndServe(port string) {
	log.Fatal(http.ListenAndServe(port, nil))
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
