package inmem

import (
	"../profile"
	"sync"
)

type inmemRepository struct {
	mtx   sync.RWMutex
	store map[int]profile.Profile
}

func (r *inmemRepository) Index() int {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	return len(r.store)
}

func (r *inmemRepository) Store(profile *profile.Profile) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	r.store[profile.Id] = *profile
	return nil
}

func (r *inmemRepository) Find(id int) (profile.Profile, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	if val, ok := r.store[id]; ok {
		return val, nil
	}
	return profile.Profile{}, profile.ErrUnknownProfile
}

func (r *inmemRepository) FindAll() []profile.Profile {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	p := make([]profile.Profile, 0, len(r.store))
	for _, val := range r.store {
		p = append(p, val)
	}
	return p
}

func (r *inmemRepository) Remove(id int) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	delete(r.store, id)
	if _, err := r.Find(id); err != nil{
		return err
	}
	return nil
}

func NewInmemRepository() profile.ProfileRepository {
	return &inmemRepository{
		store: make(map[int]profile.Profile),
	}
}
